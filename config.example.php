<?php

/**
 * Add your client API credentials if you want use sandbox environment keep debug in true
 */

return [
    'sites' => [
        'WALMART_MX' => [
            'datetime_zone' => 'America/Mexico_City',
            'client_id' => 'YOUR_CLIENT_ID',
            'client_secret' => 'YOUR_CLIENT_SECRET',
            'debug' => true
        ],
    ]
];
