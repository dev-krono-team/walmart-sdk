<?php

declare(strict_types=1);

namespace Kronoapp\Walmart\Config;

use Exception;

/**
 * Class Config
 * Load settings to client from config file in root
 *
 * @package Kronoapp\Walmart\Config
 */
final class Config
{
    const REG_NOT_VALID_CHARS = '/[@$%&\\/:*?"\'<>|~`#^+={}[\]();!]/';

    /**
     * @var array
     */
    private static $settings;

    /**
     * Loads config file
     *
     * @param string $site
     * @param string $file
     * @throws Exception
     */
    public static function loadConfig(string $site, string $file = 'config')
    {
        $file = self::parseFileName($file);
        if (!file_exists(__DIR__ . '/../../' . $file . '.php')) {
            throw new Exception('Config file not found');
        }
        $configFile = require_once __DIR__ . '/../../' . $file . '.php';

        if (!isset($configFile['sites'])) {
            throw new Exception('Invalid config file. Key sites not defined');
        }

        if (!isset($configFile['sites'][$site])) {
            throw new Exception(sprintf('Invalid config file. Key [sites][%s] not defined', $site));
        }

        if (
            !isset($configFile['sites'][$site]['client_id'])
            || !isset($configFile['sites'][$site]['client_secret'])
        ) {
            throw new Exception(sprintf('Invalid config file. Key [sites][%s][client_id and client_secret] are required', $site));
        }
        static::$settings = $configFile['sites'][$site];
    }

    /**
     * Returns all config
     *
     * @return array
     */
    public static function all(): array
    {
        return static::$settings;
    }

    /**
     * Return if debug mode
     *
     * @return bool
     */
    public static function debug(): bool
    {
        return static::$settings['debug'] ?? false;
    }

    /**
     * Parse a valid config file
     *
     * @param string $file
     * @return string
     */
    private static function parseFileName(string $file): string
    {
        return preg_replace([self::REG_NOT_VALID_CHARS, '/\.php/i'], '', $file);
    }
}