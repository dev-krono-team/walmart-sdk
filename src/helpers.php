<?php

function setShippingAddress(array $shippingInfo, string $email)
{
    $name = setValidName(array_values(array_filter(
        explode(' ', $shippingInfo['postalAddress']['name']),
        function($i) {
            return $i;
        }
    )));
    return implode('|', array(
        'FirstName: ' . $name['first_name'],
        'LastName: ' . $name['last_name'],
        'City: ' . $shippingInfo['postalAddress']['city'],
        'Address1: ' . $shippingInfo['postalAddress']['address1'],
        'Address2: ' . $shippingInfo['postalAddress']['address2'],
        'Address3: ' . $shippingInfo['postalAddress']['postalCode'],
        'Phone: ' . $shippingInfo['phone'],
        'Email: ' . $email
    ));
}

function setValidName(array $name)
{
    $index = count($name);
    if ($index > 2) {
        if ($index == 3) {
            $firstName = $name[0];
            $lastName = $name[1] . ' ' . $name[2];
        } else {
            $firstName = $name[0] . ' ' . $name[1];
            $lastName = implode(' ', array_slice($name, 2));
        }
        return [
            'first_name' => $firstName,
            'last_name' => $lastName
        ];
    } else {
        return [
            'first_name' => $name[0],
            'last_name' => $name[1] ?? null
        ];
    }
}