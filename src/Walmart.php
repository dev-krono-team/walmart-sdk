<?php

declare(strict_types=1);

namespace Kronoapp\Walmart;

use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;
use Kronoapp\Walmart\Config\Config;

/**
 * Client API Walmart
 * @package Kronoapp\Walmart
 */
class Walmart
{

    const API_VERSION = 'v3';
    const BASE_URI = 'https://marketplace.walmartapis.com/';
    const BASE_URI_SB = 'https://sandbox.walmartapis.com/';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var array
     */
    private $apiConf;

    /**
     * @var array
     */
    private $commonHeaders;

    /**
     * @var DateTimeInterface
     */
    private $datetime;

    /**
     * @var string
     */
    public $site;

    /**
     * Walmart constructor.
     * @param string $site
     * @param string $file
     * @param ClientInterface|null $client
     * @param DateTimeInterface|null $dateTime
     * @throws Exception
     */
    public function __construct(
        string $site,
        string $file,
        ClientInterface $client = null,
        DateTimeInterface $dateTime = null
    ) {
        Config::loadConfig($site, $file);
        $this->site = $site;
        $this->apiConf = Config::all();
        $this->datetime = $dateTime ?? (new DateTime())
                ->setTimezone(new DateTimeZone($this->apiConf['datetime_zone']));
        $this->commonHeaders = [
            'WM_SVC.NAME' => $site . ' Service Name',
            'WM_QOS.CORRELATION_ID' => uniqid(),
            'WM_MARKET' => 'mx',
            'Authorization' => 'Basic '
                . base64_encode(sprintf(
                    '%s:%s', $this->apiConf['client_id'], $this->apiConf['client_secret']
                )),
        ];
        if (is_null($client)) {
            $this->client = new Client([
                'base_uri' => !Config::debug() ? self::BASE_URI : self::BASE_URI_SB,
                'verify' => false,
            ]);
        } else {
            $this->client = $client;
        }
    }

    protected function getTokenApi()
    {
        unset($this->commonHeaders['WM_SEC.ACCESS_TOKEN']);
        $response = $this->client->request('POST', self::API_VERSION . '/token', [
            'form_params' => [
                'grant_type' => 'client_credentials'
            ],
            'headers' => array_merge($this->commonHeaders, ['Accept' => 'application/json'])
        ]);
        $tokenPayload = json_decode((string) $response->getBody());
        $this->commonHeaders['WM_SEC.ACCESS_TOKEN'] = $tokenPayload->access_token;
    }

    public function getItems(int $offset = 0, int $limit = 20, string $nextCursor = '*')
    {
        return $this->sendRequest('GET', 'items', [
            'offset' => $offset,
            'limit' => $limit,
            'nextCursor' => $nextCursor
        ]);
    }

    public function getItem(string $sku)
    {
        return $this->sendRequest(
            'GET',
            'items/' . $sku,
            [],
            ['Accept' => 'application/json']
        );
    }

    public function getInventory(string $sku)
    {
        return $this->sendRequest('GET', 'inventory', [
            'sku' => $sku
        ]);
    }

    public function getFeedStatus(string $feedId, $includeDetails = false, $offset = null, $limit = null)
    {
        if ($includeDetails) {
            $queryParams['includeDetails'] = 'true';
        }
        if (!is_null($offset)) {
            $queryParams['offset'] = $offset;
        }
        if (!is_null($limit)) {
            $queryParams['limit'] = $limit;
        }
        return $this->sendRequest(
            'GET',
            'feeds/' . $feedId ,
            $queryParams ?? [],
            ['Accept' => 'application/json']
        );
    }

    public function getShippingLabel($trackingNumber)
    {
        return $this->sendRequest(
            'GET',
            'orders/label/' . $trackingNumber,
            [],
            [],
            false
        );
    }

    public function getOrders(
        string $createdStartDate = null,
        string $createdEndDate = null,
        int $limit = 100,
        int $offset = 0,
        string $customerOrderId = null,
        string $purchaseOrderId = null,
        string $statusCodeFilter = 'Acknowledged'
    ) {
        $queryParams = [];
        if (null != $createdStartDate) {
            $queryParams['createdStartDate'] = $createdStartDate;
        }
        if (null != $createdEndDate) {
            $queryParams['createdEndDate'] = $createdEndDate;
        }
        if (null != $limit) {
            $queryParams['limit'] = $limit;
        }
        if (null != $offset) {
            $queryParams['offset'] = $offset;
        }
        if (null != $customerOrderId) {
            $queryParams['customerOrderId'] = $customerOrderId;
        }
        if (null != $purchaseOrderId) {
            $queryParams['purchaseOrderId'] = $purchaseOrderId;
        }
        if (null != $statusCodeFilter) {
            $queryParams['statusCodeFilter'] = $statusCodeFilter;
        }
        return $this->sendRequest('GET', 'orders',
            $queryParams,
            ['Accept' => 'application/json']
        );
    }

    public function bulk(string $path, string $feedType, $endpoint = 'feeds')
    {
        return $this->sendRequest(
            'POST',
            $endpoint,
            ['feedType' => $feedType],
            ['Accept' => 'application/json'],
            true,
            [
                'name'     => 'bulk-' . $feedType,
                'contents' => fopen($path, 'r')
            ]
        );
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $queryParams
     * @param array $headers
     * @param bool $decodeResponse
     * @param array $multipart
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendRequest(
        string $method,
        string $uri,
        array $queryParams = [],
        array $headers = [],
        bool $decodeResponse = true,
        array $multipart = []
    ) {
        $now = (new DateTime())
            ->setTimezone(new DateTimeZone($this->apiConf['datetime_zone']));
        $interval = $now->diff($this->datetime);
        try {
            if (!isset($this->commonHeaders['WM_SEC.ACCESS_TOKEN']) || $interval->i >= 14) {
                $this->getTokenApi();
            }
            $this->commonHeaders['WM_QOS.CORRELATION_ID'] = uniqid();
            $requestOptions = [
                'headers' => array_merge(
                    $this->commonHeaders,
                    !empty($headers) ? $headers : ['Content-Type' => 'application/json']
                )
            ];
            if (!empty($queryParams)) {
                $requestOptions['query'] = $queryParams;
            }
            if (!empty($multipart)) {
                $requestOptions['multipart'][] = $multipart;
            }
            $response = $this->client->request($method, self::API_VERSION . '/' . $uri, $requestOptions);
        } catch (ClientException $ex) {
            $response = $ex->getResponse();
        } catch (ServerException $ex) {
            $response = $ex->getResponse();
        }
        if (!$decodeResponse) {
            return $response->getBody()->getContents();
        }
        return json_decode($response->getBody()->getContents(), true);
    }
}